/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';



export default class App extends Component {


 

  render() {
    return (
    <View style={styles.containerStyle}>
      <Text style={styles.textStyle} onPress={() => this.myFunction()}>Hello World</Text>
    </View>
    );
  }

   // helper function 
   myFunction = () => {
    console.log("I am hungry...")
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    marginTop: 30
  },
  textStyle: {
    fontSize: 30,
    color: 'red',
    fontStyle: 'italic',
    textAlign: 'center'
  }
})

